#include "client.h"

using namespace std;

client::client(string _name, string _server_IP, string _router_IP, int _router_port){
    name = _name;
    router_port = _router_port;
    router_IP = ipv4(_router_IP);
    server_IP = ipv4(_server_IP);
    selector = new poller();
    cch = client_command_handler();
    router_connection = nullptr;
    selector->add_fd(STDIN_FILENO);
    joined_groups = vector<string>();
    group_list = vector<server_info> ();
}

void client::set_IP(std::string new_IP){
    IP = ipv4(new_IP);
}

void client::get_group_list(){
    
}

void client::join_group(string group_name){

}

void client::leave_group(string group_name){

}

void client::select_group(string group_name){
    selected_group = group_name;
}

void client::send_file(string file_name, string server_name){

}

void client::send_message(string message, string server_name){

}

void client::show_groups(){

}

void client::sync(){

}

void client::signout(){

}

void client::manage_input(){
    string line;
    getline(cin, line);
    try{
        if(cch.detect_and_run(cch.split_command(line), this) < 0)
            cout << "Your command is in wrong format!" << endl;
    }catch(const exception& e){
        cout << e.what() << endl;
    }
}

void client::manage_network_packets(){

}

void client::run(){
    vector<int> set_fds;
    while(true){
        try{
            set_fds = this->selector->poll();
        }catch(const exception& e){
            cout << e.what() << endl;
        }
        for(unsigned int i = 0; i < set_fds.size(); ++i){
            if(set_fds[i] == STDIN_FILENO)
                manage_input();
            else if((this->router_connection != nullptr) && (set_fds[i] == this->router_connection->get_fd()) )
                manage_network_packets();
        }
    }
}

int main(int argc, char const *argv[])
{
    if (argc != 5)
        cout << "error!\n";
    
    client* cl = new client(argv[1], argv[2], argv[3], stoi(argv[4]));
    cl->run();
    return 0;
}
