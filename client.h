#ifndef CLIENT_H
#define CLIENT_H

#include "config.h"
#include "ipv4.h"
#include "command_handler.h"
#include "sock.h"
#include "poller.h"

struct server_info{
    std::string server_name;
    ipv4 server_IP;
};

class client
{
private:
    std::string name;
    int router_port;
    ipv4 server_IP;
    ipv4 router_IP;
    ipv4 IP;
    ipv4 multicast_IP;
    std::string selected_group;
    client_command_handler cch;
    TCP_client_sock* router_connection;
    poller* selector;
    std::vector<server_info> group_list;
    std::vector<std::string> joined_groups;
public:
    client(std::string _name, std::string _server_IP, std::string _router_IP, int _router_port);
    void set_IP(std::string new_IP);
    void get_group_list();
    void join_group(std::string group_name);
    void leave_group(std::string group_name);
    void select_group(std::string group_name);
    void send_file(std::string file_name, std::string server_name);
    void send_message(std::string message, std::string server_name);
    void show_groups();
    void sync();
    void signout();
    void manage_input();
    void manage_network_packets();
    void run();
};


#endif