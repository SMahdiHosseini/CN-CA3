#include "command_handler.h"
#include "client.h"
#include "server.h"
#include "route_manager.h"
#include "group_server.h"
#include "router.h"
#include "link.h"

using namespace std;

vector<string> command_handler::split_command(string command){
    vector<string> commands;
    istringstream ss(command);
    string part;
    while (ss >> part)
        commands.push_back(part);
    
    return commands;
}

int client_command_handler::detect_and_run(vector<string> command, client* cl){
    if(command.size() == 0)
        return -1;

    if (command[0] == SET_IP){
        if(command.size() != 2)
            return -1;
        cl->set_IP(command[1]);
    }
    if (command[0] == GET_GROUP_LIST){
        if(command.size() != 1)
            return -1;
        cl->get_group_list();
    }   
    if (command[0] == JOIN){
        if(command.size() != 2)
            return -1;
        cl->join_group(command[1]);
    }
    if (command[0] == LEAVE){
        if(command.size() != 2)
            return -1;
        cl->leave_group(command[1]);
    }
    if (command[0] == SELECT){
        if(command.size() != 2)
            return -1;
        cl->select_group(command[1]);
    }
    if (command[0] == SEND_FILE){
        if(command.size() != 3)
            return -1;
        cl->send_file(command[1], command[2]);
    }
    if (command[0] == SEND_MESSAGE){
        if(command.size() != 3)
            return -1;
        cl->send_message(command[1], command[2]);
    }
    if (command[0] == SHOW_GROUP){
        if(command.size() != 1)
            return -1;
        cl->show_groups();
    }
    if (command[0] == SYNC){
        if(command.size() != 1)
            return -1;
        cl->sync();
    }
    if (command[0] == SINGOUT){
        if(command.size() != 1)
            return -1;
        cl->signout();
    }
    return 0;
}

int server_command_handler::detect_and_run(vector<string> command, server* serv){
    if(command.size() == 0)
        return -1;

    if (command[0] == CONNECT_ROUTER){
        if(command.size() != 2)
            return -1;
        serv->connect_to_router(stoi(command[1]));
    }

    if (command[0] == EXIT_PROGRAM){
        if(command.size() != 1)
            return -1;
        serv->exit_program();
    }
    return 0;
}

int route_manager_command_handler::detect_and_run(vector<string> command, route_manager* rm){
    if(command.size() == 0)
        return -1;
    
    if (command[0] == ROUTER){
        if(command.size() != 2)
            return -1;
        rm->create_new_router(stoi(command[1]));
    }
    if (command[0] == CONNECT){
        if(command.size() != 4)
            return -1;
        rm->connect(stoi(command[1]), stoi(command[2]), stoi(command[3]));
    }
    if (command[0] == CHANGE_COST){
        if(command.size() != 3)
            return -1;
        rm->change_cost(stoi(command[1]), stoi(command[2]));
    }
    if (command[0] == DISCONNECT){
        if(command.size() != 2)
            return -1;
        rm->disconnect(stoi(command[1]));
    }
    if (command[0] == SHOW_ROUTER){
        if(command.size() != 2)
            return -1;
        rm->show(stoi(command[1]));
    }
    if (command[0] == EXIT_PROGRAM){
        if(command.size() != 1)
            return -1;
        rm->exit_program();
    }
    return 0;
}

int group_server_command_handler::detect_and_run(vector<string> command, group_server* gpserv){
    if(command.size() == 0)
        return -1;

    if (command[0] == SET_IP){
        if(command.size() != 2)
            return -1;
        gpserv->set_IP(ipv4(command[0]));
    }
    if (command[0] == SET_IP_MULTICAST){
        if(command.size() != 2)
            return -1;
        gpserv->set_multicast_IP(ipv4(command[1]));
    }
    if (command[0] == CONNECT_ROUTER){
        if(command.size() != 2)
            return -1;
        gpserv->connect_to_router(stoi(command[1]));
    }
    if (command[0] == ADD_SERVER){
        if(command.size() != 1)
            return -1;
        gpserv->add_server();
    }
    return 0;
}

vector<string> router_command_handler::split_command(string command){
    vector<string> commands;
    istringstream ss(command);
    string part;
    while (getline(ss, part, COMMAND_DELIM))
        commands.push_back(part);
    
    return commands;
}

int router_command_handler::detect_and_run(vector<string> command, router* rtr){
    if(command.size() == 0)
        return -1;

    if (command[0] == CONNECT_LINK){
        if(command.size() != 4)
            return -1;
        rtr->connect_router(stoi(command[1]), stoi(command[2]), stoi(command[3]));
    }
    if (command[0] == COST){
        if(command.size() != 3)
            return -1;
        rtr->change_cost(stoi(command[1]), stoi(command[2]));
    }
    if (command[0] == CONNECT_ROUTER){
        if(command.size() != 2)
            return -1;
        rtr->disconnect_port(stoi(command[1]));
    }
    if (command[0] == ADD_SERVER){
        if(command.size() != 1)
            return -1;
        rtr->show();
    }
    return 0;
}