#ifndef COMMAND_HANDLER_H
#define COMMAND_HANDLER_H

#include "config.h"
class client;
class server;
class router;
class group_server;
class route_manager;

class command_handler
{
public:
    std::vector<std::string> split_command(std::string command);
};

/// client_commnad_handler
class client_command_handler : public command_handler
{
public:
    client_command_handler(){};
    int detect_and_run(std::vector<std::string>, client* cl);
};

/// server_commnad_handler
class server_command_handler : public command_handler
{
public:
    server_command_handler(){};
    int detect_and_run(std::vector<std::string>, server* ser);
};

/// router_commnad_handler
class route_manager_command_handler : public command_handler
{
public:
    route_manager_command_handler(){};
    int detect_and_run(std::vector<std::string>, route_manager* rt);
};

/// group_server_commnad_handler
class group_server_command_handler : public command_handler
{
public:
    group_server_command_handler(){};
    int detect_and_run(std::vector<std::string>, group_server* gpserv);
};

/// router_commnad_handler
class router_command_handler : public command_handler
{
public:
    router_command_handler(){};
    std::vector<std::string> split_command(std::string);
    int detect_and_run(std::vector<std::string>, router* rtr);
};

#endif