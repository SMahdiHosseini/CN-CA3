#ifndef CONFIG_H
#define CONFIG_H

#include <iostream>
#include <cstring>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <fcntl.h>
#include <algorithm>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <errno.h>
#include <arpa/inet.h>
#include <time.h>

// Constatnts
#define IPV4_SEPERATOR '.'
#define LOCAL_HOST '127.0.0.1'
#define MAX_PACKET_SIZE 2000
#define QUEUELIMIT 5
#define DATA_FOLDER "./root"
#define PIPE_WRITE_END 1
#define PIPE_READ_END 0
#define ROUTE_MANAGER_PIPE_NAME "route_manager.pipe"
#define ROUTER_FILE "router.out"
#define DEFAULT_LINK_COST 1
#define COMMAND_DELIM '#'
#define ROUTER_INITIAL_CONNECTION_PORT 40000
#define IGMP_PERIOD 15
// Commands
#define SET_IP "set_IP"
#define SET_IP_MULTICAST "set_IP_multicast"
#define GET_GROUP_LIST "‫‪get_group_list‬‬"
#define JOIN "join"
#define LEAVE "leave"
#define SELECT "select"
#define SEND_FILE "send_file"
#define SEND_MESSAGE "send_message"
#define SHOW_GROUP "show_group"
#define SYNC "sync"
#define SINGOUT "signout"
#define ROUTER "router"
#define CONNECT "connect"
#define CHANGE_COST "change_cost"
#define DISCONNECT "disconnect"
#define SHOW_ROUTER "show"
#define CONNECT_ROUTER "connect_router"
#define ADD_SERVER "add_server"
#define EXIT_PROGRAM "exit"
#define MQ "MQ"
// IPCs:
// clinet-router: Tcp socket
// router-router: Udp socket
// server-router: Tcp socket
// group_server-router: Tcp socket


// message foramts:
// MQ: MQ
#endif