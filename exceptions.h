#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>
#include <string>

class ip_format_fail : public std::exception{
    virtual const char* what() const throw(){
        return "Your Ip is not in the proper format!";
    }
};

class socket_create_fail : public std::exception{
private:
    std::string sock_type;
    int port;
public:
    socket_create_fail(std::string _sock_type, int _port) : sock_type(_sock_type), port(_port) {}
    virtual const char* what() const throw(){
        std::string message = sock_type + " Socket Creation On port " + std::to_string(port) + " failed!";
        return &message[0];
    }
};

class socket_bind_fail : public std::exception{
private:
    std::string sock_type;
    int port;
public:
    socket_bind_fail(std::string _sock_type, int _port) : sock_type(_sock_type), port(_port) {}
    virtual const char* what() const throw(){
        std::string message = sock_type + " Socket Binding On port " + std::to_string(port) + " failed!";
        return &message[0];
    }
};

class socket_listen_fail : public std::exception{
private:
    std::string sock_type;
    int port;
public:
    socket_listen_fail(std::string _sock_type, int _port) : sock_type(_sock_type), port(_port) {}
    virtual const char* what() const throw(){
        std::string message = sock_type + " Socket listening On port " + std::to_string(port) + " failed!";
        return &message[0];
    }
};

class socket_accept_fail : public std::exception{
private:
    int port;
public:
    socket_accept_fail(int _port) : port(_port) {}
    virtual const char* what() const throw(){
        std::string message = "Socket accepting On port " + std::to_string(port) + " failed!";
        return &message[0];
    }
};

class socket_connect_fail : public std::exception{
private:
    int port;
public:
    socket_connect_fail(int _port) : port(_port) {}
    virtual const char* what() const throw(){
        std::string message = "Socket connection On port " + std::to_string(port) + " failed!";
        return &message[0];
    }
};

class router_already_exists : public std::exception{
private:
    int port;
public:
    router_already_exists(int _port) : port(_port) {}
    virtual const char* what() const throw(){
        std::string message = "Router with listen port " + std::to_string(port) + " already exists!";
        return &message[0];
    }
};

class router_does_not_exists : public std::exception{
private:
    int port;
public:
    router_does_not_exists(int _port) : port(_port) {}
    virtual const char* what() const throw(){
        std::string message = "Router with listen port " + std::to_string(port) + " does not exists!";
        return &message[0];
    }
};

class link_id_already_exists : public std::exception{
private:
    int id;
public:
    link_id_already_exists(int _id) : id(_id) {}
    virtual const char* what() const throw(){
        std::string message = "Link with id " + std::to_string(id) + " already exists!";
        return &message[0];
    }
};

class link_does_not_exists : public std::exception{
private:
    int id;
public:
    link_does_not_exists(int _id) : id(_id) {}
    virtual const char* what() const throw(){
        std::string message = "Link with id " + std::to_string(id) + " does not exists!";
        return &message[0];
    }
};

class unnamed_pipe_creation_fail : public std::exception{
    virtual const char* what() const throw(){
        return "Unable to create unnamed pipe!";
    }
};

class named_pipe_creation_fail : public std::exception{
    virtual const char* what() const throw(){
        return "Unable to create named pipe!";
    }
};

class fork_fail : public std::exception{
    virtual const char* what() const throw(){
        return "Could not fork!";
    }
};

class fd_set_full : public std::exception{
    virtual const char* what() const throw(){
        return "The poll_set is full and cannot add new fd to select list!";
    }
};

class select_expired : public std::exception{
    virtual const char* what() const throw(){
        return "Select statement is expired!";
    }
};

class already_connected_to_network : public std::exception{
    virtual const char* what() const throw(){
        return "Your system is already connected to the network!";
    }
};

#endif
