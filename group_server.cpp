#include "group_server.h"

using namespace std;

group_server::group_server(std::string _name, string _server_IP){
    name = _name;
    server_IP = ipv4(_server_IP);
    selector = new poller();
    gsch = group_server_command_handler();
    router_connection = nullptr;
    users_info = vector<user_info>();
    selector->add_fd(STDIN_FILENO);
}

void group_server::set_IP(ipv4 _IP){
    this->IP = _IP;
}

void group_server::set_multicast_IP(ipv4 _multicast_IP){
    this->multicast_IP = _multicast_IP;
}

void group_server::connect_to_router(int _router_port){
    if(this->router_connection != nullptr)
        throw new already_connected_to_network();
    this->router_connection = new TCP_client_sock(_router_port);
    this->selector->add_fd(this->router_connection->get_fd() );
}

void group_server::add_server(){

}

void group_server::manage_input(){
    string line;
    getline(cin, line);
    try{
        if(gsch.detect_and_run(gsch.split_command(line), this) < 0)
            cout << "Your command is in wrong format!" << endl;
    }catch(const exception& e){
        cout << e.what() << endl;
    }
}

void group_server::manage_network_packets(){
    
}

void group_server::run(){
    vector<int> set_fds;
    while(true){
        try{
            set_fds = this->selector->poll();
        }catch(const exception& e){
            cout << e.what() << endl;
        }
        for(unsigned int i = 0; i < set_fds.size(); ++i){
            if(set_fds[i] == STDIN_FILENO)
                manage_input();
            else if((this->router_connection != nullptr) && (set_fds[i] == this->router_connection->get_fd()) )
                manage_network_packets();
        }
    }
}

int main(int argc, char* argv[]){
    group_server* srv = new group_server(argv[1], argv[2]);
    srv->run();
    return 0;
}

