#ifndef GROUP_SERVER_H
#define GROUP_SERVER_H

#include "ipv4.h"
#include "poller.h"
#include "command_handler.h"
#include "config.h"
#include "sock.h"
#include "exceptions.h"

struct user_info{
    std::string username;
    long int last_visit;
    std::vector<std::string> messages;
};

class group_server{
private:
    std::string name;
    ipv4 IP;
    ipv4 multicast_IP;
    ipv4 server_IP;
    poller* selector;
    group_server_command_handler gsch;
    std::vector<user_info> users_info;
    TCP_client_sock* router_connection;
public:
    group_server(std::string _name, std::string _server_IP);
    void set_IP(ipv4 _IP);
    void set_multicast_IP(ipv4 _multicast_IP);
    void connect_to_router(int _router_port);
    void add_server();
    void run();
    void manage_input();
    void manage_network_packets();
};


#endif