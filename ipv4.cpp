#include "ipv4.h"
#include "exceptions.h"

using namespace std;

ipv4::ipv4(string ip){
    vector<string> parts;
    istringstream ss(ip);
    string part;
    while (getline(ss, part, IPV4_SEPERATOR) )
        parts.push_back(part);
    // error detection
    if (parts.size() != 4)
        throw new ip_format_fail();
    check_parts_correctness(parts);
    // assignment
    this->first_byte = stoi(parts[0]);
    this->second_byte = stoi(parts[1]);
    this->third_byte = stoi(parts[2]);
    this->fourth_byte = stoi(parts[3]);
}

void ipv4::check_parts_correctness(vector<string> parts){
    for(auto i = parts.begin() ; i < parts.end(); ++i)
        check_part_correctness(*i);
}

void ipv4::check_part_correctness(string part){
    if(stoi(part) > 255)
        throw new ip_format_fail();
}

string ipv4::to_ip_string(){
    return to_string(first_byte) + IPV4_SEPERATOR + to_string(second_byte) + IPV4_SEPERATOR 
        + to_string(third_byte) + IPV4_SEPERATOR + to_string(fourth_byte); 
}