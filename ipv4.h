#ifndef IPV4
#define IPV4
#include "config.h"

class ipv4{
private:
    //fields
    int first_byte;
    int second_byte;
    int third_byte;
    int fourth_byte;
    // functions
    void check_parts_correctness(std::vector<std::string> parts);
    void check_part_correctness(std::string part);
public: 
    ipv4();
    ipv4(std::string ip);
    std::string to_ip_string(void);
};
#endif