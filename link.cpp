#include "link.h"

using namespace std;

Link::Link(int _id, int _router1_id, int _router2_id, 
        int _router1_fd, int _router2_fd, int _connection_port){
    this->id = _id;
    this->router1_fd = _router1_fd;
    this->router2_fd = _router2_fd;
    this->connection_port = _connection_port;
    this->cost = DEFAULT_LINK_COST;
    this->router1_id = _router1_id;
    this->router2_id = _router2_id;
}

Link::~Link(){
    string router1_disconnect_command = get_disconnect_command(this->router2_id);
    string router2_disconnect_command = get_disconnect_command(this->router1_id);
    write(router1_fd, &router1_disconnect_command[0], router1_disconnect_command.length());
    write(router2_fd, &router2_disconnect_command[0], router2_disconnect_command.length());
}

string Link::get_disconnect_command(int router_id){
    string router_command = DISCONNECT_LINK;
    router_command = router_command + COMMAND_DELIM + to_string(router_id);
    return router_command;
}

string Link::get_connect_command_to_router(int router_id){
    string router_command = CONNECT_ROUTER;
    router_command = router_command + COMMAND_DELIM + to_string(this->connection_port);
    router_command = router_command + COMMAND_DELIM + to_string(router_id);
    router_command = router_command + COMMAND_DELIM + to_string(this->cost);
    return router_command;
}

string Link::get_change_cost_command(int router_id){
    string router_command = COST;
    router_command = router_command + COMMAND_DELIM + to_string(router_id);
    router_command = router_command + COMMAND_DELIM + to_string(this->cost);
    return router_command;
}

void Link::connect_link(){
    string router1_command = get_connect_command_to_router(this->router2_id);
    string router2_command = get_connect_command_to_router(this->router1_id);
    write(router1_fd, &router1_command[0], router1_command.length());
    write(router2_fd, &router2_command[0], router2_command.length());
}

void Link::change_cost(int _new_cost){
    this->cost = _new_cost;
    string router1_change_cost_command = get_change_cost_command(this->router2_id);
    string router2_change_cost_command = get_change_cost_command(this->router1_id);
    write(router1_fd, &router1_change_cost_command[0], router1_change_cost_command.length());
    write(router2_fd, &router2_change_cost_command[0], router2_change_cost_command.length());
}