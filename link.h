#ifndef LINK_H
#define LINK_H

#include "config.h"

#define CONNECT_LINK "conn"
#define COST "cost"
#define DISCONNECT_LINK "disconn"

class Link
{
private:
    //////////////////////// Fields ////////////////
    int id;
    int connection_port;
    int cost;
    int router1_fd;
    int router2_fd;
    int router1_id;
    int router2_id;
    //////////////////////// Functions /////////////
    std::string get_connect_command_to_router(int router_id);
    std::string get_change_cost_command(int router_id);
    std::string get_disconnect_command(int router_id);
public:
    Link(int _id, int _router1_id, int _router2_id, 
        int _router1_fd, int _router2_fd, int _connection_port);
    ~Link();
    void connect_link();
    void change_cost(int _new_cost);
    void disconnect();
};

// router commands:
// conn#udp_port#router_id#cost
// cost#router_id#new_cost
// disconn#router_id
// show

#endif