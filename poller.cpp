#include "poller.h"
#include "exceptions.h"

using namespace std;

void poller::reset_set(){
    FD_ZERO(&(this->poll_set));
    for(unsigned int i = 0; i < MAX_FD_SELECT; ++i){
        if(fd_list[i] != NOTHING)
            FD_SET(fd_list[i], &(this->poll_set));
    }
}

int poller::get_next_free_slot(){
    for(unsigned int i = 0; i < MAX_FD_SELECT; ++i){
        if(fd_list[i] == NOTHING)
            return i;
    }
    throw new fd_set_full();
}

void poller::add_fd(int _fd){
    int next_free_index = get_next_free_slot();
    this->fd_list[next_free_index] = _fd;
}

void poller::remove_fd(int _fd){
    for(unsigned int i = 0; i < MAX_FD_SELECT; ++i){
        if(this->fd_list[i] == _fd)
            this->fd_list[i] = NOTHING;
    }
}
    
vector<int> poller::poll(){
    this->reset_set();
    if(select(MAX_FD_SELECT, &(this->poll_set), NULL, NULL, NULL) < 0)
        throw new select_expired();
    return get_set_fds();
}

vector<int> poller::get_set_fds(){
    vector<int> result_fds;
    for(unsigned int i = 0; i < MAX_FD_SELECT; ++i){
        if(this->fd_list[i] != NOTHING && FD_ISSET(this->fd_list[i], &(this->poll_set)))
            result_fds.push_back(this->fd_list[i]);
    }
    return result_fds;
}