#ifndef POLLER_H
#define POLLER_H
#include "config.h"

#define MAX_FD_SELECT 23
#define NOTHING -1

class poller{
private:
    //////////// Fields ///////////////
    int fd_list[MAX_FD_SELECT] = { NOTHING };
    fd_set poll_set;
    //////////// Functions ////////////
    void reset_set();
    int get_next_free_slot();
    std::vector<int> get_set_fds();
public:
    poller(){}
    void add_fd(int _fd);
    void remove_fd(int _fd);
    std::vector<int> poll();
};

#endif
