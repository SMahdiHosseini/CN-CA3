#include "route_manager.h"
#include "exceptions.h"

using namespace std;

route_manager::route_manager(){
    this->rmch = route_manager_command_handler();
    this->named_pipe_fd = this->make_named_pipe();
    this->Links = map<int, Link*>();
    this->Routers = map<int, pair<int, pid_t>>();
    this->next_connection_port = ROUTER_INITIAL_CONNECTION_PORT;
}

int route_manager::generate_connection_port(){
    int assigned_connection_port = this->next_connection_port;
    this->next_connection_port++;
    return assigned_connection_port;
}

int route_manager::make_named_pipe(){
    int fd;
    if(mkfifo(ROUTE_MANAGER_PIPE_NAME, 0666) < 0)
        throw new named_pipe_creation_fail();
    fd = open(ROUTE_MANAGER_PIPE_NAME, O_RDONLY);
    return fd;
}

void route_manager::check_router_existence(int router_port){
    if(this->Routers.find(router_port) != this->Routers.end())
        throw new router_already_exists(router_port);
}

void route_manager::check_router_non_existence(int router_port){
    if(this->Routers.find(router_port) == this->Routers.end())
        throw new router_does_not_exists(router_port);
}

void route_manager::check_link_existence(int link_id){
    if(this->Links.find(link_id) != this->Links.end())
        throw new link_id_already_exists(link_id);
}

void route_manager::check_link_non_existence(int link_id){
    if(this->Links.find(link_id) == this->Links.end())
        throw new link_does_not_exists(link_id);
}

void route_manager::make_unnamed_pipe(int* pipe_fd){
    if(pipe(pipe_fd) < 0)
        throw new unnamed_pipe_creation_fail();
}

void route_manager::start_router(int* fd_pipe, int listen_port){
    close(fd_pipe[PIPE_WRITE_END]);
    string read_side_pipe = to_string(fd_pipe[PIPE_READ_END]);
    string listen_port_str = to_string(listen_port);
    string named_pipe_str = ROUTE_MANAGER_PIPE_NAME;
    char* args[5] = {(char*)ROUTER_FILE, &listen_port_str[0], 
        &read_side_pipe[0], &named_pipe_str[0], NULL}; 
    execv(ROUTER_FILE, args);
}

void route_manager::create_new_router(int listen_port){
    pid_t pid;
    int fd_pipe[2];
    this->check_router_existence(listen_port);
    this->make_unnamed_pipe(fd_pipe);
    pid = fork();
    if(pid < 0)
        throw new fork_fail();
    else if(pid == 0)
        start_router(fd_pipe, listen_port);
    else if(pid > 0){
        close(fd_pipe[PIPE_READ_END]);
        Routers.insert(pair<int, pair<int, pid_t>>(listen_port, 
            pair<int, pid_t>(fd_pipe[PIPE_WRITE_END], pid) ));
    }
}

void route_manager::connect(int router1, int router2, int link_id){
    this->check_router_non_existence(router1);
    this->check_router_non_existence(router2);
    this->check_link_existence(link_id);
    Link* new_link = new Link(link_id, router1, router2, Routers[router1].first, Routers[router2].first, generate_connection_port() );
    new_link->connect_link();
    Links.insert(pair<int, Link*>(link_id, new_link) );
}

void route_manager::change_cost(int link_id, int new_cost){
    this->check_link_non_existence(link_id);
    Links[link_id]->change_cost(new_cost);
}

void route_manager::disconnect(int link_id){
    this->check_link_non_existence(link_id);
    delete Links[link_id];
    Links.erase(link_id);
}

void route_manager::show(int router_id){
    char response[10000];
    this->check_router_non_existence(router_id);
    string command = SHOW_ROUTER;
    write(Routers[router_id].first, &command[0], command.length());
    read(this->named_pipe_fd, response, 10000);
    cout << response;
}

void route_manager::exit_program(){
    for (auto it = Routers.begin(); it != Routers.end(); ++it){
        close((*it).second.first);
        kill((*it).second.second, SIGKILL);
    }
    exit(EXIT_SUCCESS);
}

void route_manager::run(){
    string line;
    while (getline(cin, line)){
        try{
            if(rmch.detect_and_run(rmch.split_command(line), this) < 0)
                cout << "Your command is in wrong format!" << endl;
        }catch(const exception& e){
            cout << e.what() << endl;
        }
    }
}

int main()
{
    route_manager* rm = new route_manager();
    rm->run();
    return 0;
}
