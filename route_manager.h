#ifndef ROUTE_MANAGER_H
#define ROUTE_MANAGER_H

#include "link.h"
#include "config.h"
#include "command_handler.h"

class route_manager
{
private:
    ////////////// Fields ////////////////
    route_manager_command_handler rmch;
    int named_pipe_fd; // for receiveing data from children(routers)
    // router listen_port as router_id -> (pipe_fd, pid) 
    std::map<int, std::pair<int, pid_t>> Routers;
    std::map<int, Link*> Links;
    int next_connection_port;
    ////////////// Functions ////////////////
    void check_router_existence(int router_port);
    void check_router_non_existence(int router_port);
    void check_link_existence(int link_id);
    void check_link_non_existence(int link_id);
    ///
    void make_unnamed_pipe(int* pipe_fd);
    int make_named_pipe();
    void start_router(int* fd_pipe, int listen_port);
    int generate_connection_port();
public:
    route_manager();
    void run();
    void create_new_router(int listen_port);
    void connect(int router1, int router2, int link_id);
    void change_cost(int link_id, int new_cost);
    void disconnect(int link_id);
    void show(int router_port);
    void exit_program();
};
#endif