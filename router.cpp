#include "router.h"
using namespace std;


router::router(int _listen_port, int _command_pipe_fd, string _manager_named_pipe){
    signal(SIGALRM, signal_handler);
    this->listen_port = _listen_port;
    this->manager_named_pipe = _manager_named_pipe;
    this->command_pipe_fd = _command_pipe_fd;
    this->rch = router_command_handler();
    this->tcp_connection = new TCP_server_sock(_listen_port);
    this->connected_routers = map<int, abstract_connection>();
    this->selector = new poller();
    this->selector->add_fd(_command_pipe_fd);
    this->selector->add_fd(tcp_connection->get_fd() );
    // this->instance = this;
}

void router::connect_router(int connection_port, int router_id, int _cost){
    abstract_connection _abstract_connection;
    UDP_sock* _connection = new UDP_sock(connection_port);
    _abstract_connection.connection = _connection;
    _abstract_connection.cost = _cost;
    this->connected_routers.insert(pair<int, abstract_connection>(router_id, _abstract_connection) );
    selector->add_fd(_connection->get_fd() );
}

void router::change_cost(int router_id, int new_cost){
    this->connected_routers[router_id].cost = new_cost;
}

void router::disconnect_port(int router_id){
    selector->remove_fd(this->connected_routers[router_id].connection->get_fd() );
    this->connected_routers[router_id].connection->close_socket();
    delete this->connected_routers[router_id].connection;
    this->connected_routers.erase(router_id);
}

void router::send_to_parent(string table_contents){
    int fd = open(&(this->manager_named_pipe)[0], O_WRONLY);
    write(fd, &table_contents[0], table_contents.length());
    close(fd);
}

void router::show(){
    string table_str = this->table->table_to_string();
    send_to_parent(table_str);
}

void router::manage_parent_pipe_command(){
    char buff[MAX_PACKET_SIZE];
    memset(buff, 0, MAX_FD_SELECT);
    read(this->command_pipe_fd, buff, MAX_PACKET_SIZE);
    rch.detect_and_run(rch.split_command(buff), this);
}

void router::manage_subnet_packets(){

}

void router::manage_router_packets(){

}

void router::IGMP_send_mq(){
    string packet = MQ;
    this->tcp_connection->broadcast_packet(packet);
}

void router::signal_handler(int signum){
    instance->IGMP_send_mq();
    alarm(IGMP_PERIOD);
}

void router::run(){
    vector<int> set_fds;
    alarm(IGMP_PERIOD);
    while(true){
        try{
            set_fds = this->selector->poll();
        }catch(const exception& e){
            // TODO: Error Handling and Log file 
        }
        for(unsigned int i = 0; i < set_fds.size(); ++i){
            if(set_fds[i] == this->command_pipe_fd)
                manage_parent_pipe_command();
            else if(set_fds[i] == this->tcp_connection->get_fd() )
                this->selector->add_fd(tcp_connection->accept_new_client());
            else if (this->tcp_connection->is_accepted(set_fds[i]))
                manage_subnet_packets();
            else
                manage_router_packets();
        }
    }
}

void router::set_instance(router* _instance){
    router::instance = _instance;
}

int main(int argc, char* argv[]){
    // Close Unnecessary file descriptors
    close(STDIN_FILENO);    close(STDOUT_FILENO);    close(STDERR_FILENO);
    router* rt = new router(stoi(argv[1]), stoi(argv[2]), argv[3]);
    router::set_instance(rt);
    rt->run();
    return 0;
}