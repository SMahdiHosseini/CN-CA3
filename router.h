#ifndef ROUTER_H
#define ROUTER_H
#include "config.h"
#include "command_handler.h"
#include "sock.h"
#include "poller.h"
#include "routing_table.h"

struct abstract_connection{
    int cost;
    UDP_sock* connection;
};

class router
{
private:
    ////////////// Fields ////////////////
    int listen_port; // as id
    router_command_handler rch;
    TCP_server_sock* tcp_connection;
    std::map<int, abstract_connection> connected_routers;
    int command_pipe_fd;
    poller* selector;
    std::string manager_named_pipe;
    routing_table* table;
    static router* instance;
    ////////////// Functions /////////////
    void send_to_parent(std::string table_content);
    void manage_parent_pipe_command();
    void manage_subnet_packets();
    void manage_router_packets();
public:
    router(int _listen_port, int _command_pipe_fd, std::string _manager_named_pipe);
    void connect_router(int connection_port, int router_id, int _cost);
    void change_cost(int router_id, int new_cost);
    void disconnect_port(int router_id);
    void show();
    void run();
    void IGMP_send_mq();
    static void set_instance(router* _instance);
    static void signal_handler(int signum);
};

#endif