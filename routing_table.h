#ifndef ROUTING_TABLE_H
#define ROUTING_TABLE_H

#include "config.h"

class routing_table
{
public:
    routing_table();
    std::string table_to_string();
};

#endif