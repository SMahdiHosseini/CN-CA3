#include "server.h"
#include "exceptions.h"

using namespace std;

server::server(ipv4 _IP) : IP(_IP) {
    this->router_connection = nullptr;
    this->sch = server_command_handler();
    this->selector = new poller();
    this->groups = vector<group_data>();
    this->selector->add_fd(STDIN_FILENO);
}

void server::connect_to_router(int _router_port){
    if(this->router_connection != nullptr)
        throw new already_connected_to_network();
    this->router_connection = new TCP_client_sock(_router_port);
    this->selector->add_fd(this->router_connection->get_fd() );
}

void server::exit_program(){
    if(this->router_connection != nullptr)
        this->router_connection->close_socket();
    exit(EXIT_SUCCESS);
}

void server::manage_network_packets(){

}

void server::manage_input(){
    string line;
    getline(cin, line);
    try{
        if(sch.detect_and_run(sch.split_command(line), this) < 0)
            cout << "Your command is in wrong format!" << endl;
    }catch(const exception& e){
        cout << e.what() << endl;
    }
}

void server::run(){
    vector<int> set_fds;
    while(true){
        try{
            set_fds = this->selector->poll();
        }catch(const exception& e){
            cout << e.what() << endl;
        }
        for(unsigned int i = 0; i < set_fds.size(); ++i){
            if(set_fds[i] == STDIN_FILENO)
                manage_input();
            else if((this->router_connection != nullptr) && (set_fds[i] == this->router_connection->get_fd()) )
                manage_network_packets();
        }
    }
}

int main(int argc, char* argv[]){
    server* srv = new server(ipv4(argv[1]) );
    srv->run();
    return 0;
}