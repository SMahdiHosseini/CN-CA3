#ifndef SERVER_H
#define SERVER_H
#include "ipv4.h"
#include "sock.h"
#include "config.h"
#include "command_handler.h"
#include "poller.h"

struct group_data {
    ipv4 group_ip;
    std::string name;
};

class server
{
private:
    ////////////// Fields ///////////////
    ipv4 IP;
    TCP_client_sock* router_connection;
    std::vector<group_data> groups;
    server_command_handler sch;
    poller* selector;
    ///////////// Functions /////////////
    void manage_network_packets();
    void manage_input();
public:
    server(ipv4 _IP);
    void connect_to_router(int _router_port);
    void exit_program();
    void run();
};

#endif