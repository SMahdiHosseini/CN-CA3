#include "sock.h"
#include "exceptions.h"

using namespace std;

void sock::close_socket(){
    close(this->sock_fd);
}

int sock::get_port(){
    return this->port;
}

int sock::get_fd(){
    return this->sock_fd;
}

UDP_sock::UDP_sock(int _port){
    this->port = _port;
    this->sock_fd = create_sock();
    this->bind_on_port();
}

void UDP_sock::send_packet(string packet){
    struct sockaddr_in server_address = get_common_address();
    unsigned int address_len = sizeof(server_address);
    sendto(this->sock_fd, &packet[0], MAX_PACKET_SIZE, 0, (struct sockaddr*) &server_address, address_len);
}

string UDP_sock::receive_packet(){
    char buff[MAX_PACKET_SIZE];
    memset(buff, 0, MAX_PACKET_SIZE);
    struct sockaddr_in my_address = get_common_address();
    unsigned int address_len = sizeof(my_address);
    recvfrom(this->sock_fd, buff, MAX_PACKET_SIZE, 0, (struct sockaddr*) &my_address, &address_len);
    return string(buff);
}

struct sockaddr_in UDP_sock::get_common_address(){
    struct sockaddr_in server_address;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_family = AF_INET;
    server_address. sin_port = htons(this->port);
    return server_address;
}

void UDP_sock::bind_on_port(){
    struct sockaddr_in my_address = get_common_address();
    if(bind(this->sock_fd, (struct sockaddr*) &my_address, sizeof(my_address)) < 0)
        throw new socket_bind_fail("UDP", this->port);
}

int UDP_sock::create_sock(){
    int fd;
    if((fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        throw new socket_create_fail("UDP", this->port);
    return fd;
}

int TCP_sock::create_sock(){
    int fd;
    if ((fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        throw new socket_create_fail("TCP", this->port);
    return fd;
}

struct sockaddr_in TCP_server_sock::get_my_address(){
    struct sockaddr_in srvAddr;
    srvAddr.sin_family = AF_INET; 
    srvAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    srvAddr.sin_port = htons(this->port);
    return srvAddr;
}

void TCP_server_sock::bind_on_port(){
    struct sockaddr_in my_address = get_my_address();
    if ((bind(this->sock_fd, (struct sockaddr*)&my_address, sizeof(my_address))) != 0)
        throw new socket_bind_fail("TCP", this->port);
}

void TCP_server_sock::listen_on_port(){
    if ((listen(this->sock_fd, QUEUELIMIT)) != 0)
        throw new socket_listen_fail("TCP", this->port);
}

TCP_server_sock::TCP_server_sock(int _port){
    this->port = _port;
    this->sock_fd = this->create_sock();
    this->bind_on_port();
    this->listen_on_port();
}

int TCP_server_sock::accept_new_client(){
    struct sockaddr_in clntAddr; 
    int new_client_sock;
    socklen_t len = sizeof(clntAddr);
    if (new_client_sock = accept(this->sock_fd, (struct sockaddr*) &clntAddr, &len) < 0)
        throw new socket_accept_fail(this->port);
    this->accepted.push_back(new_client_sock);
    return new_client_sock;
}

TCP_client_sock::TCP_client_sock(int _port){
    this->port = _port;
    this->sock_fd = this->create_sock();
}

struct sockaddr_in TCP_client_sock::get_server_address(){
    struct sockaddr_in srvAddr;
    srvAddr.sin_family = AF_INET; 
    srvAddr.sin_addr.s_addr = htonl(LOCAL_HOST);
    srvAddr.sin_port = htons(this->port);
    return srvAddr;
}

void TCP_client_sock::connect_to_server(){
    struct sockaddr_in srvAddr = get_server_address();
    if (connect(this->sock_fd, (struct sockaddr*) &srvAddr, sizeof(srvAddr)) != 0)
        throw new socket_connect_fail(this->port);
}

void TCP_client_sock::send_packet(string packet){
    send(this->sock_fd, &packet[0], MAX_PACKET_SIZE,0);
}

string TCP_client_sock::receive_packet(){
    char buff[MAX_PACKET_SIZE];
    memset(buff, 0, MAX_PACKET_SIZE);
    recv(this->sock_fd, buff, MAX_PACKET_SIZE, 0);
    return string(buff);
}

void TCP_server_sock::send_to(int fd, string packet){
    send(fd, &packet[0], MAX_PACKET_SIZE,0);
}

string TCP_server_sock::receive_from(int fd){
    char buff[MAX_PACKET_SIZE];
    memset(buff, 0, MAX_PACKET_SIZE);
    recv(fd, buff, MAX_PACKET_SIZE, 0);
    return string(buff);
}

bool TCP_server_sock::is_accepted(int fd){
    for (int i = 0; i < accepted.size(); i++)
        if (fd == accepted[i])
            return true;
    return false;
}

void TCP_server_sock::broadcast_packet(string packet){
    for (int i = 0; i < accepted.size(); i++)
        this->send_to(accepted[i], packet);
}