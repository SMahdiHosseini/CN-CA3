#ifndef SOCK_H
#define SOCK_H
#include "config.h"

class sock{
protected:
    int port;
    int sock_fd;
public:
    void close_socket();
    int get_port();
    int get_fd();
    virtual void send_packet(std::string packet) = 0;
    virtual std::string receive_packet() = 0;
};

class UDP_sock : public sock{
private:
    void bind_on_port();
    int create_sock();
    struct sockaddr_in get_common_address();
public:
    UDP_sock(int _port);
    virtual void send_packet(std::string packet);
    virtual std::string receive_packet();
};

class TCP_sock : public sock{
protected:
    int create_sock();
};

class TCP_server_sock : public TCP_sock{
private:
    void bind_on_port();
    void listen_on_port();
    struct sockaddr_in get_my_address();
    std::vector<int> accepted;
public:
    TCP_server_sock(int _port);
    int accept_new_client();
    void broadcast_packet(std::string send_packet);
    void send_to(int fd, std::string packet);
    std::string receive_from(int fd);
    virtual void send_packet(std::string packet){}
    virtual std::string receive_packet(){}
    bool is_accepted(int fd);
};

class TCP_client_sock : public TCP_sock{
private:
    void connect_to_server();
    struct sockaddr_in get_server_address();
public:
    TCP_client_sock(int _port);
    virtual void send_packet(std::string packet);
    virtual std::string receive_packet();
};

#endif